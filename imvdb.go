package imvdb

func New(apikey string) *Client {
	client := Client{
		apikey: apikey,
	}
	return &client
}
