package imvdb

import (
	"os"
	"testing"
)

var client *Client

func init() {
	data, err := os.ReadFile("./api.key")
	if err != nil {
		panic(err)
	}
	apikey := string(data)
	client = New(apikey)
}

func TestSearch(t *testing.T) {
	result, err := client.Search("la rappresentante di lista ciao ciao")
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	expID := 141850241511
	if result.ID != expID {
		t.Log("ID", result.ID, "!=", expID)
		t.Fail()
	}
}

func TestJSONEmptyImageMap(t *testing.T) {
	result, err := client.Find("Hard Rock Hallelujah", "Lordi")
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	expID := 210248481465
	if result.ID != expID {
		t.Log("ID", result.ID, "!=", expID)
		t.Fail()
	}
}

func TestJSONSongTitleInteger(t *testing.T) {
	result, err := client.Find("Me Ne Frego", "Achille Lauro")
	if err != nil {
		t.Log(err)
		t.Fail()
	}
	expID := 659294930286
	if result.ID != expID {
		t.Log("ID", result.ID, "!=", expID)
		t.Fail()
	}
}
