package imvdb

type MusicVideo struct {
	ID      int               `json:"id"`
	Title   string            `json:"song_title"`
	Year    int               `json:"year"`
	Images  map[string]string `json:"image"`
	Artists []Artist          `json:"artists"`
}

// Image returns the location of the original image or an empty string if no such image is present.
func (m MusicVideo) Image() string {
	if image, ok := m.Images["o"]; ok {
		return image
	}
	return ""
}

// ArtistNames returns a comma seperated list of all the names of all the artists of this MusicVideo flattened as one string. If there is no artist, an empty string is returned, if there is just one artist only that one artist's name is returned without a comma.
func (m MusicVideo) ArtistNames() string {
	if len(m.Artists) == 0 {
		return ""
	}
	if len(m.Artists) == 1 {
		return m.Artists[0].Name
	}
	result := m.Artists[0].Name
	for _, artist := range m.Artists {
		result += ", " + artist.Name
	}
	return result
}
