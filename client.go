package imvdb

import (
	"encoding/json"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
)

const baseurl = "https://imvdb.com/api/v1"

type Client struct {
	apikey string
}

// Search calls the imvdb api with the provided term and returns the first result, not doing any further checks. If you know the artist and or the song title, use Find instead.
func (c *Client) Search(term string) (*MusicVideo, error) {
	musicvideos, err := c.search(term)
	if err != nil {
		return nil, err
	}
	if len(musicvideos) < 1 {
		return nil, errNoResults
	}
	return &musicvideos[0], nil
}

// Find calls the imvdb api with the provided title and artist and returns the first result that has a title that matches the provided title and has an artist that matches the provided artist. If either title or artist is omitted, it will return any.
func (c *Client) Find(title string, artist string) (*MusicVideo, error) {
	if artist == "" && title == "" {
		return nil, errTitleOrArtistRequired
	}
	musicvideos, err := c.search(artist + " " + title)
	if err != nil {
		return nil, err
	}
	if len(musicvideos) < 1 {
		return nil, errNoResults
	}
	for _, mv := range musicvideos {
		if title == "" || strings.EqualFold(mv.Title, title) {
			for _, a := range mv.Artists {
				if artist == "" || strings.EqualFold(a.Name, artist) {
					return &mv, nil
				}
			}
		}
	}
	return nil, errNoMatchingResults
}

// search calls the imvdb api with the provided term and returns the first page of results.
func (c *Client) search(term string) ([]MusicVideo, error) {
	uri := baseurl + "/search/videos?per_page=50&q=" + url.QueryEscape(term)
	request, err := http.NewRequest(http.MethodGet, uri, nil)
	if err != nil {
		return nil, err
	}
	request.Header.Set("IMVDB-APP-KEY", c.apikey)
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}
	var results struct {
		MusicVideos []MusicVideo `json:"results"`
	}
	attempts := 0
	for err := json.Unmarshal(data, &results); err != nil && attempts < 3; {
		if typeError, ok := err.(*json.UnmarshalTypeError); ok {
			if typeError.Field == "result.image" {
				// image fix: imvdb has a bug where if there are now images for a music video it returns an empty array instead of an empty object for the "image" field.
				data = []byte(strings.ReplaceAll(string(data), `"image": []`, `"image": {}`))
			}
			if typeError.Field == "results.song_title" {
				// song_title fix: imvdb has a bug where the song title is also a valid integer, it will return it as a number instead of a string.
				exp, _ := regexp.Compile(`"song_title": (\d*)`)
				data = []byte(exp.ReplaceAllString(string(data), `"song_title": "$1"`))
			}
		}
		attempts++
	}
	return results.MusicVideos, nil
}
