package imvdb

import "errors"

var (
	errNoResults             = errors.New("no results found")
	errNoMatchingResults     = errors.New("none of the results matches the requested parameters")
	errTitleOrArtistRequired = errors.New("either title or artist is required, preferably provide both")
)
